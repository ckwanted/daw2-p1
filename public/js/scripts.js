"use strict";

$(function() {

    // SOLUCIONAR EL PROBLEMA DE CHROME Y JQUERY MOBILE
    $(document).bind('mobileinit', function(){
        $.mobile.changePage.defaults.changeHash = false;
        $.mobile.hashListeningEnabled = false;
        $.mobile.pushStateEnabled = false;
    });

    // ACTUALIZAR APPCACHE
    window.applicationCache.addEventListener('updateready', function(e) {
        if(window.applicationCache.status === window.applicationCache.UPDATEREADY) {
            window.applicationCache.swapCache();
            window.location.reload();
        }
    });

    // LAZYLOAD
    $('.lazy').Lazy({
        scrollDirection: 'vertical',
        effect: 'fadeIn'
    });

    // VERIFICAR QUE EL CORREO NO EXISTA
    $("#f_email").keyup(function(e) {

        var correo = $(this);
        var token = $("#token-crear-cuenta").val();

        $.ajax({
            url      : '/verificar',
            headers  : {'X-CSRF-TOKEN' : token},
            type     : 'POST',
            datatype : 'json',
            data     : {'email' : correo.val()}
        }).success(function(response) {

            if(response.existe) correo.css('color', 'red');
            else correo.css('color', 'black');

        });

    });

    $(".ventana-cerrar").click(function() {

        $(this).parent().fadeOut();

    });

});