<!doctype html>
<html manifest="{{asset('offline.appcache')}}">
    <head>
        <meta charset="UTF-8"/>
        <title>@yield('title', 'Daw2')</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
        <link rel="stylesheet" href="{{asset('css/jquery.mobile-1.4.5.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/estilos.css')}}"/>
        <script src="{{asset('js/jquery-1.12.1.min.js')}}"></script>
        <script src="{{asset('js/jquery.mobile-1.4.5.min.js')}}"></script>
        <script src="{{asset('js/jquery.lazy.min.js')}}"></script>
        @yield('head-extras', '')
    </head>
    <body>

        @yield('body', '')
        <script src="{{asset('js/scripts.js')}}"></script>
        @yield('body-extras', '')

    </body>
</html>