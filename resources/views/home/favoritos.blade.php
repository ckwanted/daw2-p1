@extends('master')

@section('body')

    <div data-role="page">

        @include('extras.panel.panel')

        <div data-role="header" data-position="fixed">
            <a href="#offcanvas" class="ui-btn ui-shadow ui-corner-all ui-icon-bars ui-btn-icon-notext">menu</a>
            <h1>Favoritos</h1>
        </div>

        <div role="main" class="ui-content">

            @if($favoritos)
                <div class="flex-gallery">
                    @foreach($favoritos as $favorito)
                        <div class="flex-item">
                            <div>
                                <a href="{{url('/foto', $favorito->photo_id)}}" data-ajax="false">
                                    <img class="lazy" data-src="/images/thumbnails_{{$favorito->photo->name}}" alt="{{$favorito->photo->title}}"/>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

        </div>

    </div>

@endsection