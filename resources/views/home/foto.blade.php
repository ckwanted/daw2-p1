@extends('master')

@section('body')

    <div data-role="page">

        @include('extras.panel.panel')

        <div data-role="header" data-position="fixed">
            <a href="#offcanvas" class="ui-btn ui-shadow ui-corner-all ui-icon-bars ui-btn-icon-notext">menu</a>
            <h1>Foto {{$photo->title}}</h1>
            @if($favorite)
                <a href="{{url('/delete/favoritos', $photo->id)}}" class="ui-btn ui-shadow ui-corner-all ui-icon-delete ui-btn-icon-right ui-btn-icon-notext" data-ajax="false"></a>
            @else
                <a href="{{url('/add/favoritos', $photo->id)}}" class="ui-btn ui-shadow ui-corner-all ui-icon-star ui-btn-icon-right ui-btn-icon-notext" data-ajax="false"></a>
            @endif
        </div>

        <div role="main" class="ui-content">

            <img class="foto" src="/images/{{$photo->name}}" alt="foto {{$photo->title}}"/>

        </div>

    </div>

@endsection