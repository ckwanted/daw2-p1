@extends('master')

@section('body')

    <div data-role="page">

        @include('extras.panel.panel')

        <div data-role="header" data-position="fixed">
            <a href="#offcanvas" class="ui-btn ui-shadow ui-corner-all ui-icon-bars ui-btn-icon-notext">menu</a>
            <h1>Mi Perfil</h1>
        </div>

        <div role="main" class="ui-content">

            <h3>Información Personal:</h3>

            <form id="modificar" action="{{url('/perfil/modificar', $user->id)}}" method="POST" data-ajax="false" enctype="multipart/form-data">
                <label>Nombre:
                    <input type="text" name="name" value="{{$user->name}}" required/>
                </label>
                <label>Apellido:
                    <input type="text" name="last_name" value="{{$user->last_name}}" required/>
                </label>
                <label>Contraseña:
                    <input type="password" name="password" required/>
                </label>
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="submit" value="modificar información"/>
            </form>

        </div>

    </div>

@endsection