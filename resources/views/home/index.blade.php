@extends('master')

@section('body')

    <div data-role="page">

        @include('extras.panel.panel')

        <div data-role="header" data-position="fixed">
            <a href="#offcanvas" class="ui-btn ui-shadow ui-corner-all ui-icon-bars ui-btn-icon-notext">menu</a>
            <h1>Daw2 Models</h1>
        </div>

        <div role="main" class="ui-content">

            @include('extras.home.allFotos')

        </div>

    </div>

@endsection