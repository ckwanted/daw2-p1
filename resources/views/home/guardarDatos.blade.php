@extends('simple')

@section('body-extras')
<script>
    $(function() {

        /*
         * BORRAR LOCALSTORAGE ALMACENADO ANTERIORMENTE
         */
        localStorage.clear();

        /*
         * AÑADIR NUEVOS VALORES AL LOCALSTORAGE
         */
        var imagenes = "<?php echo $imagenes; ?>".trim().split(" ");
        var base64 = "<?php echo $photoBase64; ?>".trim().split("daw2");

        localStorage.setItem("daw2-tam-photos", imagenes.length);

        for(var i = 0; i < imagenes.length; i++) {
            localStorage.setItem("daw2-photo-" + i, base64[i]);
        }

        window.location = "/";


    });
</script>
@endsection