@extends('simple')

@section('body')

    <div data-role="page">

        <div data-role="header" data-position="fixed">
            <h1>Daw2 Models</h1>
        </div>

        <div role="main" class="ui-content">

            @if(session()->has('mensaje'))
                <div class="exito">
                    <p>{{session()->get('mensaje')}}</p>
                    <span class="ventana-cerrar">X</span>
                </div>
            @elseif(session()->has('error'))
                <div class="alerta">
                    <p>{{session()->get('error')}}</p>
                    <span class="ventana-cerrar" >X</span>
                </div>
            @endif

            <div id="contenedor-login">
                <h1 class="text-center">Login</h1>
                <form action="{{url('/login')}}" method="POST" data-ajax="false">
                    <input type="email" name="email" placeholder="email" required/>
                    <input type="password" name="password" placeholder="password" required/>
                    <input id="token-login" type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="submit" value="login"/>
                </form>
                <p class="text-center">¿no tienes aun una cuenta? <a href="#crear-cuenta">registrate</a></p>
            </div>

        </div>

    </div>

    @include('extras.dialog.crear-cuenta')

@endsection
