@if($errors->any())
    <div class="alerta">
        <p>Por Favor corrige los errores:</p>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif