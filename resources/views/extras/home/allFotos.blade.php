@if($fotos)
    <div class="flex-gallery">
        @foreach($fotos as $foto)
            <div class="flex-item">
                <div>
                    <a href="{{url('/foto', $foto->id)}}" data-ajax="false">
                        <img class="lazy" data-src="/images/thumbnails_{{$foto->name}}" alt="foto {{$foto->title}}"/>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endif