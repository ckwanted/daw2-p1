<div class="ui-corner-all custom-corners">

    <div class="ui-bar ui-bar-a">
        <h3>Subir Foto</h3>
    </div>

    <div class="ui-body ui-body-a">

        @include('extras.errores')

        <form id="subir-foto" action="{{url('/subir/foto')}}" method="POST" enctype="multipart/form-data" data-ajax="false">
            <label>Titulo
                <input type="text" name="title" value="{{old('title')}}" required/>
            </label>
            <label>Foto
                <input type="file" name="photo" required/>
            </label>
            {!! csrf_field() !!}
            <input type="submit" value="subir foto"/>
        </form>

    </div>

</div>