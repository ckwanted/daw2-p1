<h3 class="ui-bar ui-bar-a ui-corner-all">Fotos</h3>

@if($misFotos)
    <div class="flex-gallery">
        @foreach($misFotos as $miFoto)
            <div class="flex-item">
                <div>
                    <a href="{{url('/foto', $miFoto->id)}}" data-ajax="false">
                        <img class="lazy" data-src="/images/thumbnails_{{$miFoto->name}}" alt="{{$miFoto->title}}"/>
                    </a>
                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                            <a href="{{url('/perfil/foto/destacada', $miFoto->id)}}" class="ui-btn" data-ajax="false">Foto Perfil</a>
                        </div>
                        <div class="ui-block-b">
                            <a href="{{url('/eliminar/foto', $miFoto->id)}}" class="ui-btn" data-ajax="false">Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif