<div data-role="panel" id="offcanvas">

    @if($user->photo)
        <img src="/images/thumbnails_{{$user->photo}}" alt="foto de perfil de {{$user->name}}"/>
    @else
        <img src="{{asset('fff.png')}}" alt="foto de perfil de {{$user->name}}"/>
    @endif

    <h3 class="text-center">{{$user->name}} {{$user->last_name}}</h3>
    <hr/>

    <h4 class="text-center">Menu</h4>

    <ul data-role="listview" data-inset="true">
        <li><a href="{{url('/')}}" class="ui-icon-home" data-ajax="false">Inicio</a></li>
        <li><a href="{{url('/perfil')}}" class="ui-icon-user" data-ajax="false">Mi Perfil</a></li>
        <li><a href="{{url('/perfil/fotos')}}" class="ui-icon-camera" data-ajax="false">Mis Fotos</a></li>
        <li><a href="{{url('/perfil/favoritos')}}" class="ui-icon-star" data-ajax="false">Favoritos</a></li>
        <li><a href="{{url('/perfil/guardar')}}" class="ui-icon-eye" data-ajax="false">Guardar En Local</a></li>
    </ul>

    <a href="{{url('/logout')}}" class="ui-btn ui-btn-icon-left ui-icon-minus" data-ajax="false">log out</a>

</div>