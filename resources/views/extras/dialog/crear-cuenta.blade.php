<div data-role="page" data-dialog="true" id="crear-cuenta">

    <div data-role="header">
        <h1>Crear una cuenta</h1>
    </div>

    <div class="ui-content" role="main">

        <form action="{{url('/registro')}}" method="POST" data-ajax="false">

            <label for="f_name">Nombre:</label>
            <input id="f_name" type="text" name="name" required/>

            <label for="f_last_name">Apellido:</label>
            <input id="f_last_name" type="text" name="last_name" required/>

            <label for="f_email">Email:</label>
            <input id="f_email" type="email" name="email" required/>

            <label for="f_password">Password:</label>
            <input id="f_password" type="password" name="password" required/>

            <input id="token-crear-cuenta" type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="submit" value="crear"/>

        </form>

    </div>

</div>