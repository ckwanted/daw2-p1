<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');

Route::get('/login', 'LoginController@getIndex');
Route::post('/login', 'LoginController@postLogin');
Route::post('/registro', 'LoginController@postRegistro');
Route::post('/verificar', 'LoginController@postVerificar');
Route::get('/logout', 'HomeController@getLogOut');

Route::get('/perfil', 'HomeController@getPerfil');
Route::get('/perfil/fotos', 'HomeController@getMisFotos');
Route::get('/perfil/favoritos/', 'HomeController@getFavoritos');
Route::get('/perfil/foto/destacada/{photo_id}', 'HomeController@getFotoPerfil');

Route::post('/perfil/modificar/{id}', 'HomeController@postModificar');

Route::post('/subir/foto', 'HomeController@postSubirFoto');
Route::get('/eliminar/foto/{id}', 'HomeController@getEliminarFoto');

Route::get('/foto/{photo_id}', 'HomeController@getFoto');
Route::get('/add/favoritos/{photo_id}', 'HomeController@getAddFavoritos');
Route::get('/delete/favoritos/{photo_id}', 'HomeController@getDeleteFavoritos');

Route::get('/perfil/guardar', 'HomeController@getGuardarDatos');
