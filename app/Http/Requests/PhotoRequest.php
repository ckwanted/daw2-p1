<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PhotoRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'photo' => 'required|mimes:jpeg,jpg,png'
        ];
    }
}
