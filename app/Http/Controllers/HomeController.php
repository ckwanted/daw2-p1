<?php

namespace App\Http\Controllers;

use App\Favorites;
use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class HomeController extends Controller {

    protected $user;

    public function __construct() {
        $this->middleware('user');
    }

    public function getIndex() {
        $user = auth()->user();
        $fotos = Photo::all();
        return view('home.index', compact('user', 'fotos'));
    }

    public function getPerfil() {
        $user = auth()->user();
        return view('home.perfil', compact('user'));
    }

    public function postModificar(Request $request, $id) {

        $userModel = User::find($id);
        $userModel->name = $request->name;
        $userModel->last_name = $request->last_name;
        $userModel->password = bcrypt($request->password);
        $userModel->updated_at = Carbon::now();
        $userModel->save();

        $this->autoGenerarAppCache();

        return redirect('/perfil');
    }

    public function getMisFotos() {
        $user = auth()->user();
        $misFotos = $user->photos;
        return view('home.misFotos', compact('user', 'misFotos'));
    }

    public function postSubirFoto(PhotoRequest $request) {

        $file = $request->file('photo');

        $title = $request->title;
        $nombreImagen = Carbon::now()->getTimestamp() . "." . $file->getClientOriginalExtension();

        $this->subirFoto($file, $nombreImagen);

        $photo = new Photo();
        $photo->title = $title;
        $photo->name = "$nombreImagen";
        $photo->user_id = auth()->user()->id;
        $photo->save();

        $this->autoGenerarAppCache();

        return redirect()->back();

    }

    public function getEliminarFoto($id) {

        $photo = Photo::find($id);

        if(auth()->user()->id == $photo->user_id) {

            DB::table('favorites')->where('photo_id', intval($id))->delete();

            $this->eliminarFoto($photo->name);
            $photo->delete();

            $this->autoGenerarAppCache();

        }

        return redirect()->back();

    }

    public function getFavoritos() {
        $user = auth()->user();
        $favoritos = $user->favorites;
        return view('home.favoritos', compact('user', 'favoritos'));
    }

    public function getAddFavoritos($photo_id) {

        $user = auth()->user();
        $favorite = new Favorites();
        $favorite->user_id = $user->id;
        $favorite->photo_id = $photo_id;
        $favorite->save();

        $this->autoGenerarAppCache();

        return redirect()->back();

    }

    public function getDeleteFavoritos($photo_id) {

        $user = auth()->user();
        DB::table('favorites')->where('user_id', $user->id)->where('photo_id', intval($photo_id))->delete();

        $this->autoGenerarAppCache();

        return redirect()->back();

    }

    public function getFotoPerfil($photo_id) {

        $photo = Photo::find($photo_id);

        if(auth()->user()->id == $photo->user_id) {
            $user = auth()->user();
            $user->photo = $photo->name;
            $user->save();
            $this->autoGenerarAppCache();
        }

        return redirect()->back();
    }

    public function getFoto($photo_id) {
        $user = auth()->user();
        $photo = Photo::find($photo_id);
        $favorite = DB::table('favorites')->where('user_id', $user->id)->where('photo_id', intval($photo_id))->count();
        return view('home.foto', compact('user', 'photo', 'favorite'));
    }

    public function getGuardarDatos() {

        $photos = Photo::all();
        $imagenes = "";
        $photoBase64 = "";


        foreach($photos as $photo) {
            $photoBase64 .= base64_encode(file_get_contents("images/$photo->name")) . "daw2";
            $imagenes .= $photo->name . " ";
        }

        return view('home.guardarDatos', compact('imagenes', 'photoBase64'));

    }

    public function getLogOut() {
        auth()->logout();
        $this->autoGenerarAppCache();
        return redirect('/login');
    }








    private function subirFoto($file, $nombreImagen) {
        Storage::disk('local')->put($nombreImagen, File::get($file));

        $imagenOptimizada = Image::make("images/$nombreImagen");
        $imagenOptimizada->save("images/$nombreImagen");

        $thumbnails = Image::make("images/$nombreImagen");
        $thumbnails->resize(400, 400);
        $thumbnails->save("images/thumbnails_$nombreImagen");
    }

    private function eliminarFoto($nombreImagen) {
        Storage::delete(["thumbnails_$nombreImagen", "$nombreImagen"]);
    }

    private function autoGenerarAppCache() {

        $vecAppCache = file(public_path() . '/offline.appcache.ORIGINAL');
        $gestor = fopen(public_path() . '/offline.appcache', 'w+');
        $i = 0;
        $fecha = Carbon::now();

        foreach ($vecAppCache as $linea) {

            $i++;

            if($i === 2) fwrite($gestor, "# $fecha v1\n");
            else fwrite($gestor, $linea);

        }

        fclose($gestor);

    }

}
