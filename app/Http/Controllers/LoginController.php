<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller {

    public function __construct() {
        $this->middleware('guest');
    }

    public function getIndex() {
        return view('login.index');
    }

    public function postLogin(Request $request) {

        $datos = [
            'email'    => $request->email,
            'password' => $request->password
        ];

        if(auth()->attempt($datos, false)) {
            return redirect('/');
        }

        return redirect()->back()->with('error','correo o contraseña incorrecta ...');

    }

    public function postRegistro(Request $request) {

        if($this->existeCorreo($request->email)) {
            return redirect('/login')->with(['error' => 'El correo ya existe']);
        }

        $user = User::create($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        $data = [
            'nombre'    => $request->name,
            'apellido'  => $request->last_name
        ];

        \Mail::send('extras.email.nuevo-usuario', $data, function($message) use ($request) {

            $message->from(env('CONTACT_MAIL'), 'Daw2 Models');

            $message->subject('Bienvenido a Daw2 Models');

            $message->to($request->email, $request->name);

        });

        return redirect('/login')->with('mensaje', 'creado correctamente ...');

    }

    public function postVerificar(Request $request) {

        if($request->ajax()) {
            return response()->json([
                'existe' => $this->existeCorreo($request->email)
            ]);
        }

    }


    private function existeCorreo($email) {
        return User::all()->where('email', $email)->count();
    }

}
