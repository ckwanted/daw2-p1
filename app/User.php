<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'users';

    protected $fillable = ['name', 'last_name', 'email', 'password', 'photo'];

    protected $hidden = ['password', 'remember_token'];

    public function photos() {
        return $this->hasMany(Photo::class);
    }

    public function favorites() {
        return $this->hasMany(Favorites::class);
    }

}
