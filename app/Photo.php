<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = ['title', 'name', 'user_id'];

    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function favorites() {
        return $this->hasMany(Favorites::class);
    }

}
